import scala.io.StdIn.readLine
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.nio.file.{Files, Paths, StandardOpenOption}
import scala.jdk.CollectionConverters._
import scala.util.{Failure, Success, Try}
import scala.annotation.tailrec

object TodoList {

case class Entry(id: Int, todo: String, category: String, deadline: LocalDate, completed: Boolean = false) {
  def complete: Entry = this.copy(completed = true)
}
  def main(args: Array[String]): Unit = {
    val getTodos = getFromFile()
    val updatedEntries = interface(getTodos)
    println("hello")
    saveEntries(updatedEntries)
  }

  private def displayTodos(entries: List[Entry]): Unit = {
    if (entries.nonEmpty) {
      entries.foreach { entry =>
        println(s"ID: ${entry.id}, Todo: ${entry.todo}, Category: ${entry.category}, Deadline: ${entry.deadline}, Completed: ${entry.completed}")
      }
    } else {
      println("No Entries")
    }
  }

  private def addEntry(entries: List[Entry], todo: String, category: String, deadline: LocalDate): List[Entry] = {
    val tempId = if (entries.isEmpty) 1 else entries.map(_.id).max + 1
    entries :+ Entry(tempId, todo, category, deadline)
  }

  private def completeEntry(entries: List[Entry], id: Int): List[Entry] = {
    entries.map {
      case entry if entry.id == id => entry.complete
      case entry => entry
    }
  }

  private def updateEntry(entries: List[Entry], id: Int, todo: String, category: String, deadline: LocalDate): List[Entry] = {
    entries.map {
      case entry if entry.id == id => entry.copy(todo = todo, category = category, deadline = deadline)
      case entry => entry
    }
  }

  private def deleteEntry(entries: List[Entry], id: Int): List[Entry] = {
    entries.filterNot(_.id == id)
  }


  private def searchEntries(entries: List[Entry], input: String): List[Entry] = {
    entries.filter(entry => entry.todo.contains(input) || entry.category.contains(input))
  }

  private def categoryFilter(entries: List[Entry], category: String): List[Entry] = {
    entries.filter(_.category == category)
  }

  private def deadlineFilter(entries: List[Entry], deadline: LocalDate): List[Entry] = {
    entries.filter(_.deadline.isEqual(deadline))
  }

  private def getFromFile(): List[Entry] = {
    val path = Paths.get("todos.txt")
    if (Files.exists(path)) {
      val lines = Files.readAllLines(path)
      lines.asScala.toList.flatMap { line =>
        val splits = line.split(",")
        if (splits.length != 0) {
          Try {
            val id = splits(0).toInt
            val todo = splits(1)
            val category = splits(2)
            val deadline = LocalDate.parse(splits(3), DateTimeFormatter.ofPattern("yyyy-MM-dd"))
            val completed = splits(4).toBoolean
            Some(Entry(id, todo, category, deadline, completed))
          }.toOption.flatten.toList
        } else {
          List.empty[Entry]
        }
      }
    } else {
      println("fuckfest")
      List.empty[Entry]
    }
  }

  private def saveEntries(entries: List[Entry]): Unit = {
    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    val content = entries.map(entry => s"${entry.id},${entry.todo},${entry.category},${entry.deadline.format(formatter)},${entry.completed}").mkString("\n")
    Files.write(Paths.get("todos.txt"), content.getBytes, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING)
  }

  @tailrec
  private def interface (entries: List[Entry]): List[Entry] = {
    println("===== To-Do List Program =====")
    println("1. View Todo")
    println("2. Add Todos")
    println("3. Complete Todo")
    println("4. Update Todo")
    println("5. Delete Todo")
    println("6. Search")
    println("7. Filter Category")
    println("8. Filter Deadline")
    println("9. Save")


    val input = readLine("Select Option: ")
    val result = input match {
      case "1" =>
        displayTodos(entries)
        entries
      case "2" =>
        val todo = readLine("Enter todo: ")
        val category = readLine("Enter category: ")
        val deadline = readLine("Enter deadline as yyyy-mm-dd: ")
        Try(LocalDate.parse(deadline)) match {
          case Success(deadline) => addEntry(entries, todo, category, deadline)
          case Failure(_) =>
            println("Enter date as yyyy-MM-dd")
            entries
        }
      case "3" =>
        val id = Try(readLine("Enter id: ").toInt).getOrElse(-1)
        if (entries.exists(_.id == id)) {
          completeEntry(entries, id)
        } else {
          println("Entry not found")
          entries
        }
      case "4" =>
        val id = Try(readLine("Enter id: ").toInt).getOrElse(-1)
        if (entries.exists(_.id == id)) {
          val todo = readLine("Enter updated todo: ")
          val category = readLine("Enter updated category: ")
          val deadline = readLine("Enter updated deadline as yyyy-MM-dd: ")
          Try(LocalDate.parse(deadline)) match {
            case Success(deadline) => updateEntry(entries, id, todo, category, deadline)
            case Failure(_) =>
              println("Enter date as yyyy-MM-dd")
              entries
          }
        } else {
          println("Entry not found")
          entries
        }
      case "5" =>
        val id = Try(readLine("Enter id: ").toInt).getOrElse(-1)
        if (entries.exists(_.id == id)) {
          deleteEntry(entries, id)
        } else {
          println("Entry not found")
          entries
        }
      case "6" =>
        val query = readLine("Enter todo: ")
        val results = searchEntries(entries, query)
        displayTodos(results)
        entries
      case "7" =>
        val category = readLine("Enter category: ")
        val results = categoryFilter(entries, category)
        displayTodos(results)
        entries
      case "8" =>
        val deadlineStr = readLine("Enter deadline as yyyy-MM-dd): ")
        Try(LocalDate.parse(deadlineStr)) match {
          case Success(deadline) =>
            val results = deadlineFilter(entries, deadline)
            displayTodos(results)
          case Failure(_) =>
            println("Enter date as yyyy-MM-dd")
        }
        entries
      case "9" =>
        entries
      case _ =>
        println("Enter a number between 1-9")
        entries
    }
    if (result == "9") {
      saveEntries(result)
    }
    interface(result)
  }

}
